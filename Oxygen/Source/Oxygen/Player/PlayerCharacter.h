// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "PlayerCharacter.generated.h"

/**
 *
 */
UCLASS()
class OXYGEN_API APlayerCharacter : public APaperCharacter
{
	GENERATED_BODY()
public:
	APlayerCharacter();


protected:
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

private:
	void moveRight(float value);
	void moveForward(float value);
	void beginSprint();
	void endSprint();
	void zoomIn();
	void zoomOut();
private:
	float sprintMultiplier = 1.f;
	class USpringArmComponent* cameraBoom;
	UPROPERTY(EditAnywhere)
	class UCameraComponent* camera;

	const float minCameraDistance = 100.f;
	const float maxCameraDistance = 1000.f;
	const float zoomSpeed = 10.f;

};
