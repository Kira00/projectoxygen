// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"


APlayerCharacter::APlayerCharacter() : Super()
{
	// Create a camera boom attached to the root (capsule)
	cameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("cameraBoom"));
	cameraBoom->SetupAttachment(RootComponent);
	cameraBoom->TargetArmLength = 500.0f;
	cameraBoom->SocketOffset = FVector(0.0f, 0.0f, 0.0f);
	cameraBoom->AddLocalRotation(FQuat(FRotator(0, 0, 90)));
	cameraBoom->SetUsingAbsoluteRotation(true);
	cameraBoom->bDoCollisionTest = false;
	cameraBoom->SetRelativeRotation(FRotator(0.0f, 0, 0.0f));

	camera = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	camera->ProjectionMode = ECameraProjectionMode::Orthographic;
	camera->OrthoWidth = 2048.f;
	camera->SetupAttachment(cameraBoom, USpringArmComponent::SocketName);
}

void APlayerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	//Axises
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::moveRight);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::moveForward);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerCharacter::beginSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APlayerCharacter::endSprint);
}

void APlayerCharacter::moveRight(float value)
{
	AddMovementInput(FVector(1.f, 0.f, 0.f), value * sprintMultiplier);
}

void APlayerCharacter::moveForward(float value)
{
	AddMovementInput(FVector(0.f, 1.f, 0.f), value * sprintMultiplier);
}

void APlayerCharacter::beginSprint()
{
	sprintMultiplier = 2.f;
}

void APlayerCharacter::endSprint()
{
	sprintMultiplier = 1.f;
}

void APlayerCharacter::zoomIn()
{
	cameraBoom->TargetArmLength = cameraBoom->TargetArmLength - zoomSpeed >= minCameraDistance ? cameraBoom->TargetArmLength - zoomSpeed : minCameraDistance;
}

void APlayerCharacter::zoomOut()
{
	cameraBoom->TargetArmLength = cameraBoom->TargetArmLength + zoomSpeed <= maxCameraDistance ? cameraBoom->TargetArmLength - zoomSpeed : maxCameraDistance;
}


