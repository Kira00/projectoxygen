// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Environment/Terrain.h"
#include "Player/PlayerCharacter.h"
#include "OxygenGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class OXYGEN_API AOxygenGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOxygenGameModeBase();

	void BeginPlay();

private:

	void spawnPlayer();
	void createMap();
private:
	UPROPERTY(EditAnywhere)
	int sizeX = 100;
	UPROPERTY(EditAnywhere)
	int sizeY = 100;

	FString seed;

	UPROPERTY()
	ATerrain* map;
	APlayerCharacter* character;
};
