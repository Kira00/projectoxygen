// Copyright Epic Games, Inc. All Rights Reserved.


#include "OxygenGameModeBase.h"


int NR_FIELDS = 3;

AOxygenGameModeBase::AOxygenGameModeBase() : Super()
{

}

void AOxygenGameModeBase::BeginPlay()
{
	createMap();
	//spawnPlayer();
}

void AOxygenGameModeBase::spawnPlayer()
{
	FVector position(1696, 1760, 0);
	character = GetWorld()->SpawnActor<APlayerCharacter>(position, FRotator());

}

void AOxygenGameModeBase::createMap()
{
	int startTileX = rand() % NR_FIELDS;
	int startTileY = rand() % NR_FIELDS;

	int seedNumber = (int)strtol("TEST", NULL, 16);
	srand(seedNumber);
	int startX = 0, startY = 0;
	for (size_t i = 0; i < NR_FIELDS; i++)
	{
		for (size_t j = 0; j < NR_FIELDS; j++)
		{
			map = GetWorld()->SpawnActor<ATerrain>();

			if (startTileX ==j && startTileY == i) {
				map->createMap(startX, startY, true, "TEST", sizeX, sizeY);
			}
			else
			{
				map->createMap(startX, startY, false, "TEST", sizeX, sizeY);
			}
			startX = map->getFieldDimension() * (j + 1);
		}
		startY = map->getFieldDimension() * (i + 1);
		startX = 0;
	}


}
