// Fill out your copyright notice in the Description page of Project Settings.


#include "ResourceField.h"

AResourceField::AResourceField() {
	// ToDo: Get Field types

	fieldTypes.push_back(TEXT("Dirt"));
	fieldTypes.push_back(TEXT("Coal"));

}


void AResourceField::createBaseMap(int startX, int startY,int width, int height)
{
	if (width < 1 || height < 1) return;
	UWorld* world = GetWorld();

	FVector location = FVector(startX, startY, 0);
	FRotator rotation = FRotator(0, 0, 90);
	ABaseField* newTile = nullptr;
	
	for (size_t i = 0; i < height; i++)
	{
		::std::vector<ABaseField*> fields;
		for (size_t j = 0; j < width; j++)
		{
			newTile = world->SpawnActor<ABaseField>(location, rotation);
			newTile->initialize(TEXT("Dirt"));
			fields.push_back(newTile);
			location.X += newTile->getSpriteX();
		}
		baseFields.push_back(fields);
		location.X = startX;
		location.Y += newTile->getSpriteY();
	}
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString("Basemap Spawned"));
}

void AResourceField::createResourceField(FString seed, Map baseField, int startX, int startY) {
	UWorld *world = GetWorld();
	//ToDo: Get Resource Fields from configuration file
	
	FRotator rotation = FRotator(0, 0, 90);
	AResourceTile* newTile; 

	FVector location = FVector(startX, 0, startY);
	for (size_t i = 0; i < defaultFieldSize; i++)
	{
		::std::vector<ABaseField*> fields(defaultFieldSize);

		for (size_t j = 0; j < defaultFieldSize; j++)
		{
			int randomNumber = rand() % 10;
			FString tileType;
			switch (randomNumber)
			{
			case 0:
			case 1:
			case 2:
			case 3:
			case 4: tileType = TEXT("Dirt"); break;
			case 5:
			case 6:
			case 7: tileType = TEXT("Coal"); break;
			case 8:
			case 9: tileType = TEXT("Dirt"); break;

			default:
				tileType = TEXT("Dirt");
				break;
			}

			//startY ? startY + defaultFieldSize <= height : height - defaultFieldSize;
			//startY ? startX + defaultFieldSize <= width : width - defaultFieldSize;
			
			auto field = baseField[startY + i][startX + j];

			newTile = world->SpawnActor<AResourceTile>(field->GetActorLocation(), rotation);
			newTile->setBaseTile(field);
			newTile->initialize(tileType);
			newTile->getBaseField()->deactivate();
			fields.push_back(newTile);
		}
		resourceFields.push_back(fields);
	}
}

void AResourceField::createResourceStartField(FString seed, Map baseField, int startX, int startY)
{
	UWorld* world = GetWorld();
	//ToDo: Get Resource Fields from configuration file

	FRotator rotation = FRotator(0, 0, 90);
	AResourceTile* newTile;

	FVector location = FVector(startX, 0, startY);
	for (size_t i = 0; i < defaultFieldSize; i++)
	{
		::std::vector<ABaseField*> fields(defaultFieldSize);

		for (size_t j = 0; j < defaultFieldSize; j++)
		{
			int randomNumber = rand() % 10;
			FString tileType;

			//Limit available resources for start zone
			switch (randomNumber)
			{
			case 0:
			case 1:
			case 2:
			case 3:
			case 4: tileType = TEXT("Dirt"); break;
			case 5:
			case 6:
			case 7: tileType = TEXT("Coal"); break;
			case 8:
			case 9: tileType = TEXT("Dirt"); break;

			default:
				tileType = TEXT("Dirt");
				break;
			}

			//startY ? startY + defaultFieldSize <= height : height - defaultFieldSize;
			//startY ? startX + defaultFieldSize <= width : width - defaultFieldSize;

			auto field = baseField[startY + i][startX + j];

			newTile = world->SpawnActor<AResourceTile>(field->GetActorLocation(), rotation);
			newTile->setBaseTile(field);
			newTile->initialize(tileType);
			newTile->getBaseField()->deactivate();
			fields.push_back(newTile);
		}
		resourceFields.push_back(fields);
	}
}

void AResourceField::createLiquidField(FString seed, Map baseField, int startX, int startY)
{
	UWorld* world = GetWorld();
	//ToDo: Get Resource Fields from configuration file

	FRotator rotation = FRotator(0, 0, 90);
	ALiquidTile* newTile;

	FVector location = FVector(startX, 0, startY);
	for (size_t i = 0; i < defaultFieldSize; i++)
	{
		::std::vector<ABaseField*> fields(defaultFieldSize);

		for (size_t j = 0; j < defaultFieldSize; j++)
		{
			int randomNumber = rand() % 10;
			FString tileType;
			if ((i == 0 || i == defaultFieldNumber -1) || (j == 0 || j == defaultFieldNumber - 1))
			{
				switch (randomNumber)
				{
				case 0:
				case 1:
				case 2:
				case 3:tileType = TEXT("Coal"); break;
				case 4:
				case 5:
				case 6:
				case 7: 
				case 8:
				case 9:

				default:
					tileType = TEXT("Dirt");
					break;
				}
			}
			else
			{
				switch (randomNumber)
				{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:tileType = TEXT("Coal"); break;
				case 8:
				case 9:

				default:
					tileType = TEXT("Dirt");
					break;
				}
			}

			auto field = baseField[startY + i][startX + j];

			newTile = world->SpawnActor<ALiquidTile>(field->GetActorLocation(), rotation);
			newTile->setBaseTile(field);
			newTile->initialize(tileType);
			newTile->getBaseField()->deactivate();
			fields.push_back(newTile);
		}
		resourceFields.push_back(fields);
	}

}

