// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ResourceTile.h"
#include "LiquidTile.generated.h"


UENUM(BlueprintType)
enum class EShoreline : uint8 {
	//Stright shore
	ES_Left UMETA(DisplayName="Left"),
	ES_Top UMETA(DisplayName="Top"),
	ES_Right UMETA(DisplayName="Right"),
	ES_Bottom UMETA(DisplayName="Bottom"),

	//Corner shore
	ES_TopLeft UMETA(DisplayName="TopLeft"),
	ES_TopRight UMETA(DisplayName="TopRight"),
	ES_BottomLeft UMETA(DisplayName="BottomLeft"),
	ES_BottomRight UMETA(DisplayName="BottomRight")
};



/**
 * 
 */
UCLASS()
class OXYGEN_API ALiquidTile : public AResourceTile
{
	GENERATED_BODY()

public:
	ALiquidTile();

	void setShoreline(EShoreline shoreLine);
private:
	bool isShoreLine;

};
