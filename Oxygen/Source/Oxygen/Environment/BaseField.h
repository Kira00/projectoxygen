// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include <map>

#include "PaperSpriteComponent.h"
#include "Papersprite.h"

#include "PaperSpriteActor.h"
#include "BaseField.generated.h"

/**
 * 
 */
UCLASS()
class OXYGEN_API ABaseField : public APaperSpriteActor
{
	GENERATED_BODY()
public:
	explicit ABaseField();
	void initialize(FString terrainField);
	int getSpriteX();
	int getSpriteY();
	void deactivate();
private:

private:
	::std::map<FString, UPaperSprite*> spriteMap;

	UPROPERTY(EditAnywhere)
	UPaperSpriteComponent* tileRenderComponent;
};
