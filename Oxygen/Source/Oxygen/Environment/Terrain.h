// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperSpriteActor.h"
#include "PaperGroupedSpriteActor.h"
#include "ResourceField.h"
#include "Terrain.generated.h"

//UPROPERTY(Edit_Anywhere)
constexpr int STANDARD_CHUNK_SIZE{ 64 };
//constexpr int defaultFieldSize{ 9 };
/**
 * 
 */
UCLASS()
class OXYGEN_API ATerrain : public APaperGroupedSpriteActor
{
	GENERATED_BODY()
	
public:
	ATerrain() = default;

	void createMap(int xPostion, int yPosition, bool isStartingField, ::std::string seed, int width, int height);
	int getFieldDimension();
private:
	//Methodes
	bool checkFieldUsed(::std::vector<int>& usedX, ::std::vector<int>& usedY, int startX, int startY);
private: //Variables
	struct Chunk
	{
		int sizeX{ STANDARD_CHUNK_SIZE };
		int sizeY{ STANDARD_CHUNK_SIZE };
		Chunk(int size) {
			sizeX = size;
			sizeY = size;
		}
	};
	UPROPERTY()
	AResourceField *field;

	UPROPERTY()
	AResourceField* baseMap;

};
