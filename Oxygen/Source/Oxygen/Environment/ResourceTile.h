// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "BaseField.h"

#include "PaperSpriteActor.h"
#include "Papersprite.h"

#include "ResourceTile.generated.h"


/**
 * 
 */
UCLASS()
class OXYGEN_API AResourceTile : public ABaseField
{
	GENERATED_BODY()

public:
	AResourceTile();

	void setBaseTile(ABaseField* field);
	ABaseField* getBaseField() { return baseTile; }
private:
	
private:
	UPROPERTY()
	ABaseField* baseTile;
};
