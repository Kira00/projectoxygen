// Fill out your copyright notice in the Description page of Project Settings.


#include "Terrain.h"

void ATerrain::createMap(int xPosition, int yPosition, bool isStartingField, ::std::string seed, int width, int height)
{
	baseMap = GetWorld()->SpawnActor<AResourceField>();
	baseMap->createBaseMap(xPosition,yPosition,width, height);
	field = GetWorld()->SpawnActor<AResourceField>();
	auto nrFields = rand() % 10;

	::std::vector<int> usedX;
	::std::vector<int> usedY;

	for (size_t i = 0; i < nrFields; i++)
	{
		auto startX = rand() % width;
		auto startY = rand() % height;
		if (startX + defaultFieldSize >= width) {
			startX = width - defaultFieldSize;
		}
		if (startY + defaultFieldSize >= height) {
			startY = height - defaultFieldSize;
		}

		auto fieldUsed = checkFieldUsed(usedX, usedY, startX, startY);


		if (!fieldUsed)
		{
			if (isStartingField) {
				field->createResourceStartField(FString(seed.c_str()), baseMap->getBaseFields(), startX, startY);
			}
			else {
				field->createResourceField(FString(seed.c_str()), baseMap->getBaseFields(), startX, startY);
			}
			for (size_t z = 0; z < defaultFieldSize; z++)
			{
				usedX.push_back(startX + z);
				usedY.push_back(startY + z);
			}

		}
		else {
			i--;
		}
	}
	if(!isStartingField) {
		for (size_t i = 0; i < 2; i++)
		{
			auto startX = rand() % width;
			auto startY = rand() % height;
			if (startX + defaultFieldSize >= width) {
				startX = width - defaultFieldSize;
			}
			if (startY + defaultFieldSize >= height) {
				startY = height - defaultFieldSize;
			}

			auto fieldUsed = checkFieldUsed(usedX, usedY, startX, startY);

			if (!fieldUsed) {
				field->createLiquidField(FString(seed.c_str()), baseMap->getBaseFields(), startX, startY);
				for (size_t z = 0; z < defaultFieldSize; z++)
				{
					usedX.push_back(startX + z);
					usedY.push_back(startY + z);
				}
			}
			else {
				i--;
			}
		}
	}
	
}

int ATerrain::getFieldDimension()
{
	return baseMap->getBaseFields()[0].size() * baseMap->getBaseFields()[0][0]->getSpriteX();
}

bool ATerrain::checkFieldUsed(::std::vector<int>& usedX, ::std::vector<int>& usedY, int startX, int startY)
{
	bool used = false;
	for (size_t i = 0; i < defaultFieldSize; i++)
	{
		auto itY = std::find(usedY.begin(), usedY.end(), startY + i);
		if (itY != usedY.end())
		{
			for (size_t j = 0; j < defaultFieldSize; j++)
			{
				auto itX = std::find(usedX.begin(), usedX.end(), startX + j);

				if (itX != usedX.end())
				{
					used = true;
					j = defaultFieldSize;
					i = defaultFieldSize;
				}
			}
		}
	}
	::std::sort(usedX.begin(), usedX.end());
	usedX.erase(std::unique(usedX.begin(), usedX.end()), usedX.end());
	::std::sort(usedY.begin(), usedY.end());
	usedY.erase(std::unique(usedY.begin(), usedY.end()), usedY.end());
	return used;
}
