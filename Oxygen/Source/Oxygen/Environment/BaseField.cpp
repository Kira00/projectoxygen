// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseField.h"

ABaseField::ABaseField()
{
	// ToDo: Move into seperate Class
	ConstructorHelpers::FObjectFinder<UPaperSprite> dirtSprite(TEXT("PaperSprite'/Game/Terrain/Spr_Terr_00_Soil_Sprite.Spr_Terr_00_Soil_Sprite'"));
	ConstructorHelpers::FObjectFinder<UPaperSprite> coalSprite(TEXT("PaperSprite'/Game/Terrain/Spr_Terr_00_Coal_Sprite.Spr_Terr_00_Coal_Sprite'"));
	spriteMap["Dirt"] = dirtSprite.Object;
	spriteMap["Coal"] = coalSprite.Object;

	//flipBook = CreateDefaultSubobject<UPaperFlipbook>(TEXT("Flipbook"));
	//flipBook.;

	//GetRenderComponent()->SetSprite(spriteMap["Dirt"]);
	tileRenderComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("PaperSpriteComponent"));
	//renderComponent->SetMobility(EComponentMobility::Movable);
	tileRenderComponent->SetWorldLocation(GetActorLocation());
	tileRenderComponent->SetupAttachment(RootComponent);
}

void ABaseField::initialize(FString terrainField)
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString("INIT"));
	tileRenderComponent->SetSprite(spriteMap[terrainField]);
}

int ABaseField::getSpriteX()
{
	return tileRenderComponent->GetSprite()->GetSourceSize().X;
}

int ABaseField::getSpriteY()
{
	return tileRenderComponent->GetSprite()->GetSourceSize().Y;
}

void ABaseField::deactivate()
{
	tileRenderComponent->SetVisibility(false);
	SetActorHiddenInGame(true);
	tileRenderComponent->SetHiddenInGame(true);
	SetActorEnableCollision(false);
	SetActorTickEnabled(false);
}
