// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include <vector>

#include "BaseField.h"

#include "PaperGroupedSpriteActor.h"
#include "ResourceTile.h"
#include "LiquidTile.h"
#include "ResourceField.generated.h"


constexpr int defaultFieldSize{ 9 };
constexpr int defaultFieldNumber{ 5 };

using Map = ::std::vector<::std::vector<ABaseField*>>;

/**
 * 
 */
UCLASS()
class OXYGEN_API AResourceField : public APaperGroupedSpriteActor
{
	GENERATED_BODY()
public:
	AResourceField();

	void createBaseMap(int startX, int startY,int width, int height);
	void createResourceField(FString seed, Map baseField, int startX, int startY);
	void createResourceStartField(FString seed, Map baseField, int startX, int startY);
	void createLiquidField(FString seed, Map baseField, int startX, int startY);

	Map getBaseFields() { return baseFields; }

private:
	::std::list<FString> fieldTypes;

	Map baseFields;
	Map resourceFields;
	Map liquidFields;

};
